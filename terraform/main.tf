terraform {
	required_providers {
		openstack = {
			source = "terraform-providers-openstack/openstack"
			version = "~> 1.35.0"
		}
	}
}

provider "openstack" {
	tenant_name = "admin"
	user_name = "admin"
	password = "7427f51ae1154c88"
	auth_url = "http://10.0.2.15:5000"
	region = "RegionOne"
}

resource "openstack_images_image_v2" "images" {
	name = "img_cirros_test"
	region = "RegionOne"
	local_file_path = "./cirros-0.5.2-x86_64-disk.img"
	verify_checksum = "false"
	container_format = "bare"
	disk_format = "iso"
	protected = "false"
	properties = {
		"description" = "First upload test"
		"image" = "cirros"
	}
}
