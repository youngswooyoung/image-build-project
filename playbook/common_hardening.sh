#!/bin/bash


#############public image hardening#######
################ U-01 ###################
#Import pam_securetty
echo "----------------------------U-01------------------------------------"

if grep -wq "auth       required     pam_securetty.so" /etc/pam.d/login; then
  echo" U-01 (/etc/pam.d/login) enabled"
else  
  echo "auth       required     pam_securetty.so" >> /etc/pam.d/login
  echo" U-01 (/etc/pam.d/login) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

################ U-05 ###################

echo "----------------------------U-05------------------------------------"

if grep -wq "PATH=.:$PATH:$HOME/bin" /etc/profile; then
  sed -i 's/PATH=.:$PATH:$HOME\/bin/PATH=$PATH:$HOME\/bin:./g'
  echo "U-05 (/etc/profile) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

################ U-06 ###################

echo "----------------------------U-06------------------------------------"

for files in $(find / -user root -type f \( -perm -04000 -o -perm -02000 \) -xdev -exec ls -al {} \; | awk '{print $9}')
do
  if [ -f $files ]; then
    rm -rf $files
    echo "U-06 ($files) enabled"
  fi
done

echo -e "--------------------------------------------------------------------\n"

################ U-07 ###################
#/etc/passwd owner/permission check

echo "----------------------------U-07------------------------------------"

if ! [ $(stat -c "%A" /etc/passwd) == "-rw-r--r--" ]; then
  chown root /etc/passwd  
  chmod 644 /etc/passwd
  echo "U-07 (/etc/passwd) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

################ U-08 ###################
#/etc/shadow owner/permission check

echo "----------------------------U-08------------------------------------"

if ! [ $(stat -c "%A" /etc/shadow) == "-r--------" ]; then
  chown root /etc/shadow
  chmod 400 /etc/shadow
  echo "U-08 (/etc/shadow) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

################ U-09 ###################
#/etc/hosts owner/permission check

echo "----------------------------U-09------------------------------------"

if ! [ $(stat -c "%A" /etc/hosts) == "-rw-------" ]; then
  chown root /etc/hosts
  chmod 600 /etc/hosts
  echo "U-09 (/etc/hosts) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

################ U-10 ###################
#/etc/inetd.conf owner/permission check

echo "----------------------------U-10------------------------------------"

if [ -f /etc/inetd.conf ]; then 
  chown root /etc/inetd.conf
  chmod 600 /etc/inetd.conf
  echo "U-10(/etc/inetd.conf) enabled."
fi

#/etc/xinetd.conf owner/permission check
if [ -f /etc/xinetd.conf ]; then
  chown root /etc/xinetd.conf
  chmod 600 /etc/xinetd.conf
  echo "U-10(/etc/xinetd.conf) enabled."
fi

echo -e "--------------------------------------------------------------------\n"

################ U-11 ###################   

echo "----------------------------U-11------------------------------------"

#/etc/syslog.conf owner/permission check
if [ -f /etc/syslog.conf ]; then
  chown root /etc/syslog.conf
  chmod 640 /etc/syslog.conf
  echo "U-11(/etc/syslog.conf) enabled."
fi

# /etc/rsyslog.conf owner/permission check (CentOS > 6)
if [ -f /etc/rsyslog.conf ]; then
  chown root /etc/rsyslog.conf
  chmod 640 /etc/rsyslog.conf
  echo "U-11(/etc/rsyslog.conf) enabled."
fi
#- name: /etc/rsyslog.conf owner/permission check (CentOS > 6)

echo -e "--------------------------------------------------------------------\n"

################ U-12 ###################   
#/etc/services owner/permission check

echo "----------------------------U-12------------------------------------"

if [ -f /etc/services ]; then
  chown root /etc/services
  chmod 644 /etc/services
  echo "U-12(/etc/services) enabled."
fi

echo -e "--------------------------------------------------------------------\n"

################ U-13 ###################   

echo "----------------------------U-13------------------------------------"

for files in $(find / -user root -type f \( -perm -04000 -o -perm -02000 \) -xdev -exec ls -al {} \; | awk '{print $9}')
do
  chmod ugo-s $files
done
echo "U-13 enabled"

echo -e "--------------------------------------------------------------------\n"

################ U-14 ###################   
################ U-15 ###################   
#Find world writable file permission check

echo "----------------------------U-15------------------------------------"

for files in $(find / -path /proc -prune -o -type f -perm -2 -exec ls -l {} \; | awk '{print $9}')
do
  chmod o-w $files
done
echo "U-15 enabled"

echo -e "--------------------------------------------------------------------\n"

################ U-16 ###################

echo "----------------------------U-16------------------------------------"

for files in $(find /dev -type f -exec ls -l {} \;)
do
  chown root $files
  chmod 600 $files
done
echo "U-16 enabled" 

echo -e "--------------------------------------------------------------------\n"

################ U-17 ################### 

echo "----------------------------U-17------------------------------------"

if [ -f /etc/host.equiv ]; then
  chown root /etc/host.equiv
  chmod 600 /etc/host.equiv
  echo "U-17 (/etc/host.equiv) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

################ U-55 ###################

echo "----------------------------U-55------------------------------------"

if [ -f /etc/hosts.lpd ]; then
  chown root /etc/hosts.lpd
  chmod 600 /etc/hosts.lpd
  echo "U-55 (/etc/hosts.lpd) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

################ U-57 ###################  

echo "----------------------------U-57------------------------------------"

if grep -wq "umask 022\nexport umask" /etc/profile; then
  echo "umask 022" >> /etc/profile
  echo "export 022" >> /etc/profile
  echo "U-57 (/etc/profile) enabled"
fi 

echo -e "--------------------------------------------------------------------\n"

################ U-22 ###################

echo "----------------------------U-22------------------------------------"

#cron file(/etc/cron.allow) owner/permission check
if [ -f /etc/cron.allow ]; then
  chown root /etc/cron.allow
  chmod 640 /etc/cron.allow
  echo "U-22 (/etc/cron.allow) enabled"
fi
#cron file(/etc/cron.deny) owner/permission check
if [ -f /etc/cron.deny ]; then
  chown root /etc/cron.deny
  chmod 640 /etc/cron.deny
  echo "U-22 (/etc/cron.deny) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

################ U-26 ###################
#Disable automountd service

echo "----------------------------U-26------------------------------------"

service automountd status
if [ $? = 0 ]; then
  systemctl stop automountd
  systemctl disable automountd
  echo "U-26 (automountd) enabled"
fi

service autofs status
if [ $? = 0 ]; then
  systemctl stop autofs
  systemctl disable autofs
  echo "U-26 (autofs) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

################ U-60 ###################
#enable sshd service

echo "----------------------------U-60------------------------------------"

service sshd status
if [ $? = 0 ]; then
  systemctl start sshd
  systemctl enable sshd
  echo "U-26 (sshd) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

################ U-65 ###################
#at file(/etc/at.allow) owner/permission check

echo "----------------------------U-65------------------------------------"

if [ -f /etc/at.allow ]; then
  chown root /etc/at.allow
  chmod 640 /etc/at.allow
  echo "U-65 (/etc/at.allow) enabled"
fi

#at file(/etc/at.deny) owner/permission check
if [ -f /etc/at.deny ]; then
  chown root /etc/at.deny
  chmod 640 /etc/at.deny
  echo "U-65 (/etc/at.deny) enabled"
fi

echo -e "--------------------------------------------------------------------\n"



############finance image hardening############
###############SRV-004#####################
#Disable sendmail service

echo "---------------------------SRV-004------------------------------------"

service sendmail status
if [ $? = 0 ]; then
  systemctl stop sendmail 
  systemctl disable sendmail
  echo "SRV-004 (sendmail) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

###############SRV-013#####################
#Disable ftp service

echo "---------------------------SRV-013------------------------------------"

service vsftpd status
if [ $? = 0 ]; then
  systemctl stop vsftpd
  systemctl disable vsftpd
  echo "SRV-013 (vsftpd) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

###############SRV-015#####################
#Disable nfs service (Redhat)

echo "---------------------------SRV-015------------------------------------"

service nfsd status
if [ $? = 0 ]; then
  systemctl stop nfsd
  systemctl disable nfsd
  echo "SRV-015 (nfsd) enabled"
fi

#  when: (ansible_facts['distribution']] == "RedHat") or (ansible_facts['distribution']] == "CentOS")

#Disable nfs service ()
service nfs-kernel-server status
if [ $? = 0 ]; then
  systemctl stop nfs-kernel-server
  systemctl disable nfs-kernel-server
  echo "SRV-015 (nfs-kernel-server) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

#  when: (ansible_facts['distribution']] == "Debian") or (ansible_facts['distribution']] == "Ubuntu")


###############SRV-016#####################
#Disable rpc service

echo "---------------------------SRV-016------------------------------------"

service rpcbind status
if [ $? = 0 ]; then
  systemctl stop rpcbind
  systemctl disable rpcbind
  echo "SRV-016 (rpcbind) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

###############SRV-026#####################

echo "---------------------------SRV-026------------------------------------"

if grep -wq "PermitRootLogin no" /etc/ssh/sshd_config; then
  echo "SRV-026 (/etc/ssh/sshd_config) enabled"
else
  sed -i 's/PermitRootLogin no/PermitRootLogin/g'
  echo "SRV-026 (/etc/ssh/sshd_config) enabled"
fi 
 
#Set securetty file
if grep -wq "console\nvc/1\nvc/2\ntty1\ntty2\nttyS0" myfile; then
  echo "SRV-026 (/etc/securetty) enabled"
else
  echo "console\nvc/1\nvc/2\ntty1\ntty2\nttyS0" >> /etc/securetty
  echo "SRV-026 (/etc/securetty) enabled"
fi 

echo -e "--------------------------------------------------------------------\n"

###############SRV-028#####################
#Setting profile shell timeout

echo "---------------------------SRV-028------------------------------------"

if grep -wq "TMOUT=900" myfile; then
    echo "SRV-028 (/etc/profile) enabled"
else
    echo "TMOUT=900" >> /etc/profile
    echo "SRV-028 (/etc/profile) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

###############SRV-035#####################
#vulnerable service kill(Redhat - chkconfig) #Redhat chkconfig command incude echo, discard, chargen, daytime

echo "---------------------------SRV-035------------------------------------"

for services in "tftp" "echo-dgram" "echo-stream" "discard-dgram" "discard-stream" "chargen-dgram" "chargen-stream" "daytime-dgram" "daytime-stream"
do
  chkconfig --list $services
  if [ $? = 0 ]; then
    chkconfig $services off
    echo "SRV-035 ($services) enabled"
  fi
done
#- name: vulnerable service kill(Debian) --> need to check in OS(Ubuntu) /etc/xinetd and 'disable = yes'

for services in "tftp" "echo-dgram" "echo-stream" "discard-dgram" "discard-stream" "chargen-dgram" "chargen-stream" "daytime-dgram" "daytime-stream"
do
#  chkconfig --list $services
#  if [ $? = 0 ]; then
    update-rc.d -f $services remove
    echo "SRV-035 ($services) enabled"
#  fi
done



#vulnerable service kill (Redhat - service stop) #Redhat service include NIS, talk, ntalk, tftp, rsh, rlogin, rexec

for services in "ypserv" "ypbind" "ntalk" "talk" "tftp.socket" "rsh.socket" "rlogin.socket" "rexec.socket"
do
  service $services status
  if [ $? = 0 ]; then
    systemctl stop $services
    systemctl disable $services
    echo "SRV-035 ($services) enabled"
  fi
done

#vulnerable service kill (Debian - service stop, package remove)

for services in "nis" "ntalk" "talk" "tftp" "rsh-client" "rsh-redone-client"
do
  service $services status
  if [ $? = 0 ]; then
    systemctl stop $services
    systemctl disable $services
    apt remove $services
    echo "SRV-035 ($services) enabled"
  fi
done


echo -e "--------------------------------------------------------------------\n"

###############SRV-048#####################
#Disable web service (Redhat)

echo "---------------------------SRV-048------------------------------------"

service httpd status
if [ $? = 0 ]; then
  systemctl stop httpd
  systemctl disable httpd
  echo "SRV-048 (httpd) enabled"
fi
#  when: (ansible_facts['distribution']] == "RedHat") or (ansible_facts['distribution']] == "CentOS")

#Disable web service (Debian)
service apache2 status
if [ $? = 0 ]; then
  systemctl stop apache2
  systemctl disable apache2
  echo "SRV-048 (apache2) enabled"
fi


#  when: (ansible_facts['distribution']] == "Debian") or (ansible_facts['distribution']] == "Ubuntu")

echo -e "--------------------------------------------------------------------\n"

###############SRV-070#####################

###############SRV-081#####################
#crontab files owner/permission check

echo "---------------------------SRV-081------------------------------------"

if [ -d /var/spool/cron/crontab ]; then
  chown -R root /var/spool/cron/crontab
  chmod -R 640 /var/spool/cron/crontab
  echo "SRV-081 (/var/spool/cron/crontab) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

###############SRV-082#####################
#system main directories permission check

echo "---------------------------SRV-082------------------------------------"

for dir in "/usr" "/bin" "/sbin" "/etc" "/var"
do
  if [ -d $dir ]; then
    chmod o-w $dir
    echo "SRV-082 ($dir) enabled"
  fi
done

echo -e "--------------------------------------------------------------------\n"

###############SRV-083#####################
#system startup script permission check

echo "---------------------------SRV-083------------------------------------"

if [ -f /etc/syslogd.conf ]; then
  chmod o-w /etc/rc.d/rc.local
  echo "SRV-083 (/etc/rc.d/rc.local) enabled"
fi

if [ -f /etc/syslogd.conf ]; then
  chmod o-w /etc/rc.local
  echo "SRV-083 (/etc/rc.local) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

###############SRV-084#####################
#system main files owner/permission check (0644)

echo "---------------------------SRV-084------------------------------------"

if [ -f /etc/syslogd.conf ]; then
  chmod 644 /etc/syslogd.conf
  echo "SRV-084 (/etc/syslogd.conf) enabled"
fi
if [ -f /etc/services ]; then
  chmod 644 /etc/services
  echo "SRV-084 (/etc/services) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

###############SRV-087#####################
#C compiler permission check

echo "---------------------------SRV-087------------------------------------"

for files in $(which gcc)
do
  if [ -f $files ]; then
    chmod o-x $files
    echo "SRV-087 ($files) enabled"
  fi
done

echo -e "--------------------------------------------------------------------\n"

###############SRV-091#####################
# if put in together 'u-s' and 'g-s' in mode, it occur errors

echo "---------------------------SRV-091------------------------------------"

for files in "/sbin/dump" "/usr/bin/lpq-lpd" "/usr/bin/newgrp" "/sbin/restore" "/usr/bin/lpr" "/usr/sbin/lpc" "/sbin/unix_chkpwd" "/usr/bin/lpr-lpd" "/usr/sbin/lpc-lpd" "/usr/bin/at" "/usr/bin/lprm" "/usr/sbin/traceroute" "/usr/bin/lpq" "/usr/bin/lprm-lpd"
do
  if [ -f $files ]; then
    chmod ug-s $files
    chmod o-x $files
    echo "SRV-091 ($files) enabled"
  fi
done

echo -e "--------------------------------------------------------------------\n"

###############SRV-092#####################

###############SRV-093#####################
##already exist task on U-15

###############SRV-096#####################
##already exist task on U-57


###############SRV-108#####################
#/var/log directory permission check

echo "---------------------------SRV-108------------------------------------"

if [ -d /var/log ]; then
  chmod -R 644 /var/log
  echo "SRV-108 (/var/log) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

###############SRV-109#####################
#su log setting with auditd

echo "---------------------------SRV-109------------------------------------"

if [ -f /etc/audit/audit.rules ]; then
  echo "-w /var/log/sudo.log -p wa -k actions" >> /etc/audit/audit.rules
  echo "SRV-109 (/etc/audit/audit.rules) enabled"
fi
#su log setting with auditd restartd

service auditd status
if [ $? = 0 ]; then
  systemctl restart auditd
  systemctl enable auditd
  echo "SRV-109 (auditd) enabled"
fi
    
echo -e "--------------------------------------------------------------------\n"

###############SRV-131#####################
#/etc/pam.d/su line add

echo "---------------------------SRV-131------------------------------------"

if ! [ grep -wq "auth required pam_wheel.so use_uid" /etc/pam.d/su ]; then
  echo "auth required pam_wheel.so use_uid" >> /etc/pam.d/su
  echo "SRV-131 (/etc/pam.d/su) enabled"
fi

echo -e "--------------------------------------------------------------------\n"

###############SRV-147#####################
#Disable snmp service

echo "---------------------------SRV-147------------------------------------"

service snmpd status
if [ $? = 0 ]; then
  systemctl stop snmpd
  systemctl disable snmpd
  echo "SRV-147  (snmpd) enabled"
fi 

echo -e "--------------------------------------------------------------------\n"

###############SRV-158#####################
#Disable telnet service (Redhat)

echo "---------------------------SRV-158------------------------------------"

service telnetd status
if [ $? = 0 ]; then
  systemctl stop telnetd
  systemctl disable telnetd
  echo "SRV-158 (telnetd) enabled"
fi
#  when: (ansible_facts['distribution']] == "RedHat") or (ansible_facts['distribution']] == "CentOS")

#- name: Disable telnet service (Debian)

echo -e "--------------------------------------------------------------------\n"

###############SRV-174#####################
#Disable DNS service (Redhat)

echo  "---------------------------SRV-174------------------------------------"

service telnetd status
if [ $? = 0 ]; then
  systemctl stop named
  systemctl disable named
  echo "SRV-174 (named) enabled"
fi
#  when: (ansible_facts['distribution']] == "RedHat") or (ansible_facts['distribution']] == "CentOS")

#Disable DNS service (Debian)
service bind9 status
if [ $? = 0 ]; then
  systemctl stop bind9
  systemctl disable bind9
  echo "SRV-174 (bind9) enabled"
fi
#  when: (ansible_facts['distribution']] == "Debian") or (ansible_facts['distribution']] == "Ubuntu")

echo -e "--------------------------------------------------------------------\n"

